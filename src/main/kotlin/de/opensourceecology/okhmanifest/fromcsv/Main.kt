// SPDX-FileCopyrightText: 2020 Robin Vobruba <hoijui.quaero@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

package de.opensourceecology.okhmanifest.fromcsv

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import com.xenomachina.argparser.mainBody
import java.io.File
import java.util.regex.Pattern

val RGX_VARIABLE : Pattern = Pattern.compile("\\$\\{([^}]++)}")
val RGX_IMG_URL : Pattern = Pattern.compile(" \\(([^)]+)\\)$")

/**
 * Read a CSV table, and convert each row into it's own OKH YAML manifest file.
 */
fun main(args: Array<String>) = mainBody {
    println("Converting CSV table to YAML manifest files ...")

    ArgParser(args).parseInto(::ParsedArgs).run {

        checkNotNull(outputDir) { "Please provide an output directory with -o or --output" }

        convert(outputDir!!, ymlTemplateFile, csvInputFile)
    }
}

class ParsedArgs(parser: ArgParser) {

//    val verbose by parser.flagging(
//        "-v", "--verbose",
//        help = "enable verbose mode")

    val outputDir : File? by parser.storing(
        "-o", "--output",
        help = "path to the output directory") { File(this) }
        .default<File?>(null)

    val ymlTemplateFile by parser.storing(
        "-t", "--template",
        help = "YAML output template file path") { File(this) }
        .default(File("src/main/resources/from-csv-template.yml"))

    val csvInputFile by parser.storing(
        "-c", "--csv-file",
        help = "CSV input file path") { File(this) }
        .default(File("src/main/resources/input.csv"))
}

fun convert(outputDir: File, ymlTemplateFile: File, csvInputFile: File) {

    println("Writing YAML manifest files to: ${outputDir.absolutePath}")
    outputDir.mkdirs()

    val ymlTemplate = ymlTemplateFile.readText()

    // Read all with header
    val rows = csvReader().readAllWithHeader(csvInputFile)

    // Do some processing line by line
    for (row in rows) {
        val mapped = createMapping(row)

        val ymlManifest = substituteAllTokens(mapped, ymlTemplate)
        val manifestFileName = "okh-" + row["Part No."] + ".yml"

        //println(ymlManifest)
        File(outputDir, manifestFileName).writeText(ymlManifest)
    }
}

fun createMapping(row : Map<String, String>) : Map<String, String> {

    val mapped = HashMap<String, String>()

    // TODO Convert these into using ".?let ..."
    mapped["title"] = row["Part No."] + " - " + row["Name"]
    mapped["description"] = "" + row["Description"]
    mapped["license.documentation"] = "" + row["Licence"]
    mapped["date-created"] = "" + row["Start Date"]
    mapped["date-updated"] = "" + row["End Date"]
    mapped["version"] = "" + row["Version"]
    row["Qty produced"]?.let {
        val made = (it.isNotEmpty() && (it.toInt() > 0))
        mapped["made"] = made.toString()
    }
    // TODO Check if we can synthesize this out of other values
    //mapped["development-stage"] = "" + row["???"]

    row.filter { it.key.endsWith(" Image") and it.value.isNotEmpty() }.map { it.value }.toList().firstOrNull()?.let { imgStr: String ->
        val imgUrlMatcher = RGX_IMG_URL.matcher(imgStr)
        if (imgUrlMatcher.find()) {
            imgUrlMatcher.group(1)?.let { imgUrl: String ->
                mapped["image"] = imgUrl
            }
        } else {
            println("WARNING: Failed extracting image URL from '$imgStr'")
        }
    }

    // TODO We need at least one of these two
    //mapped["project-link"] = "" + row["???"]
    //mapped["documentation-home"] = "" + row["???"]

    val contributors : MutableSet<String> = HashSet()
    row["Designer"]?.let { contributors.add(it) }
    row["Maker"]?.let { contributors.add(it) }
    contributors.remove("")
    mapped["contributors"] = ""
    contributors.forEach { mapped["contributors"] = "  - name: $it\n" }

    row["Designer"]?.let {
        mapped["licensor.name"] = it
        mapped["licensor.affiliation"] = "???" // TODO HACK Fixed value
    }

    return mapped
}

/**
 * Replaces all instances of strings like {@code "${XXX}"}
 * with {@code tokensMap["XXX"]} in a given string {@code toInspect}.
 * If {@code "XXX"} is not found in {@code tokensMap},
 * then it is left as {@code "${XXX}"} in the string {@code toInspect}.
 * @see "https://stackoverflow.com/a/17462527/586229"
 */
fun substituteAllTokens(tokensMap: Map<String, String>, toInspect: String): String {

    val matcher = RGX_VARIABLE.matcher(toInspect)
    var result = toInspect
    while (matcher.find()) {
        // Ex: ${fizz}
        val token = matcher.group()
        // Ex: fizz
        val tokenKey = matcher.group(1)

        tokensMap[tokenKey]?.let {
            result = result.replaceFirst(Pattern.quote(token).toRegex(), it)
        }
    }

    return result
}
