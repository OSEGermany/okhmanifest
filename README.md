<!--
SPDX-FileCopyrightText: 2020 Robin Vobruba <hoijui.quaero@gmail.com>

SPDX-License-Identifier: CC0-1.0
-->

# OKH Manifest Converter

[![License: GPL v3](
    https://img.shields.io/badge/License-GPLv3-blue.svg)](
    https://www.gnu.org/licenses/gpl-3.0)
[![REUSE status](
    https://api.reuse.software/badge/gitlab.com/OSEGermany/okhmanifest)](
    https://api.reuse.software/info/gitlab.com/OSEGermany/okhmanifest)

> **NOTE**\
> This is of little practical use by now (Dec. 2021).
> You may want to have a look at
> [the `okh-tool`](https://github.com/OPEN-NEXT/LOSH-OKH-tool)
> instead.

This is the Open Know-How Manifest converter.
It converts from one format of an OKH manifest to an other.
It is written in Kotlin, which runs on the Java Virtual Machine (JVM).

You may want to use the
[**latest build artifacts**](https://osegermany.gitlab.io/okhmanifest/)
in your own CI builds,\
so you do not have to compile them each time from scratch.

## Compiling

You need [Maven 3](https://maven.apache.org/install.html) installed on your system.

On linux, this is usually the `maven` package,
so on Ubuntu or Debian you can install it with:

```bash
apt-get install maven
```

Then compile with:

```bash
mvn package
```

## Conversions

### From CSV table to YAML

while developing:

```bash
mvn exec:java -Dexec.args="--output $(pwd)/target/output/"
```

out there:

```bash
java -jar target/*-jar-with-dependencies.jar \
    --output "$(pwd)/target/output/"
```

You should now have the manifest files in _target/output/_.
